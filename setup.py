from setuptools import setup
from cli import version

setup(
    name="hedgedoc-notebook",
    version=version.VERSION,
    packages=["cli", "journal", "search"],
    install_requires=[
        "click >= 8.1.3",
        "thefuzz >= 0.19.0",
        "python-levenshtein >= 0.12.2"
    ],
    entry_points="""
        [console_scripts]
        hedgedoc-notebook=cli.__main__:cli
    """
)
