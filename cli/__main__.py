"""CLI main."""
import click
from cli.version import VERSION
from journal import monthly_index
import search as matcher

# Matcher ratio used for search results.
MATCHER_RATIO = 75


@click.group()
@click.version_option(version=VERSION)
def cli():
    """hedgedoc-notebook CLI"""


@click.command()
@click.argument("month")
@click.argument("year")
@click.argument("file")
def journal_index_month(month, year, file):
    """Create journal index for a month."""
    click.echo("Generating month index...")
    monthly_index.generate_index(month, int(year), file)
    click.echo(f"Wrote the index in {file}")


@click.command()
@click.argument("path")
@click.argument("keyword")
def search(path, keyword):
    """Search for a keyword in the files in the given path."""
    click.echo(f"Searching for '{keyword}' in '{path}' ...")
    results = matcher.match_in_directory(path, keyword, MATCHER_RATIO)
    if len(results) == 0:
        click.echo("No matches found")
    else:
        click.echo("\nFound matches in files:")
        for result in results:
            click.echo(result)


cli.add_command(journal_index_month)
cli.add_command(search)

if __name__ == "__main__":
    cli()
