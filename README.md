# hedgedoc-notebook

Tools to help use hedgedoc as notebook.

## Development Setup

Ensure [pipenv](https://pipenv.pypa.io/) and [tox](https://tox.wiki) are
installed on the system.

Use pipenv to create an environment and install the dependencies:

```console
$ pipenv install -d
```

Activate the pipenv created environment and use it:

```console
$ pipenv shell
Launching subshell in virtual environment...
...
(hedgedoc-notebook) $
```

Install the CLI in development mode:

```console
$ pipenv install -e .
Installing -e ....
Adding hedgedoc-notebook to Pipfile's [packages]...
✔ Installation Succeeded
...
To activate this project's virtualenv, run pipenv shell.
Alternatively, run a command inside the virtualenv with pipenv run.

$ pipenv run hedgedoc-notebook
Usage: hedgedoc-notebook [OPTIONS] COMMAND [ARGS]...

  hedgedoc-notebook CLI

Options:
  --version  Show the version and exit.
  --help     Show this message and exit.

Commands:
  journal-index-month  Create journal index for a month.
```

### Testing

Run the tests using tox:

```console
$ tox
...
test_content_generator (tests.test_journal.TestContentGenerator) ... ok
test_generate_index (tests.test_journal.TestGenerateIndex) ... ok
test_month_to_index (tests.test_journal.TestMonthToIndex) ... ok

----------------------------------------------------------------------
Ran 3 tests in 0.001s

OK
```

In the above, pipenv uses the virtualenv provided by tox and installs the
dependencies in the environment.

**NOTE:** Run tox outside the pipenv environment in order to run tests in
virtual environments created by tox.
