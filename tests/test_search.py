"""Search related functionality tests."""
import os
import unittest
from dataclasses import dataclass
from typing import List
import search


class TestSearch(unittest.TestCase):
    """Test search functions."""

    def test_match(self):
        """Test content matcher."""
        content = "aaaaabbbbbcccc ddddeee ffffggg 1133 4467888"

        @dataclass
        class TestCase:
            """testcase"""
            keyword: str
            ratio: int
            result: bool

        cases = [
            TestCase(keyword="aaa", ratio=75, result=False),
            TestCase(keyword="aaaaaa", ratio=75, result=False),
            TestCase(keyword="xxx", ratio=75, result=False),
            TestCase(keyword="ccdd", ratio=75, result=False),
            TestCase(keyword="DDDDEEE", ratio=75, result=True),
            TestCase(keyword="DDddEee", ratio=75, result=True),
            TestCase(keyword="1133", ratio=75, result=True),
        ]

        for case in cases:
            with self.subTest(case=case):
                result = search.match(content, case.keyword, case.ratio)
                self.assertEqual(result, case.result)

    def test_match_in_file(self):
        """Test file content matcher."""
        target_dir = "./tests/testdata/notes"

        @dataclass
        class TestCase:
            """testcase"""
            keyword: str
            ratio: int
            filename: str
            result: bool

        cases = [
            TestCase(keyword="python", ratio=75,
                     filename="1.md", result=True),
            TestCase(keyword="golang", ratio=75,
                     filename="2.md", result=True),
            TestCase(keyword="ruby", ratio=75,
                     filename="1.md", result=False),
            TestCase(keyword="dolore magna", ratio=75,
                     filename="5.md", result=True),
            TestCase(keyword="dolore zzz", ratio=75,
                     filename="5.md", result=True),
            TestCase(keyword="dolqqq zzz", ratio=75,
                     filename="5.md", result=False),
        ]

        for case in cases:
            with self.subTest(case=case):
                filepath = os.path.join(target_dir, case.filename)
                result = search.match_in_file(
                    filepath, case.keyword, case.ratio)
                self.assertEqual(result, case.result)

    def test_match_in_directory(self):
        """Test directory file content matcher."""
        target_dir = "./tests/testdata/notes"

        @dataclass
        class TestCase:
            """testcase"""
            keyword: str
            ratio: int
            result: List[str]

        cases = [
            TestCase(keyword="python", ratio=75, result=[
                './tests/testdata/notes/1.md',
                './tests/testdata/notes/3.md',
                './tests/testdata/notes/4.md']),
            TestCase(keyword="golang", ratio=75, result=[
                './tests/testdata/notes/2.md',
                './tests/testdata/notes/4.md']),
            TestCase(keyword="python golang", ratio=75, result=[
                './tests/testdata/notes/4.md']),
            TestCase(keyword="ruby", ratio=75, result=[])
        ]

        for case in cases:
            with self.subTest(case=case):
                result = search.match_in_directory(
                    target_dir, case.keyword, case.ratio)
                self.assertEqual(result, case.result)
