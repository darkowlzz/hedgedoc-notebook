"""A collection of search related utilities."""
import os
from thefuzz import fuzz


def match(content, keyword, ratio):
    """Fuzz match the keyword in the content and return the results based on the
    given ratio. The match is case insensitive."""
    keyword = keyword.lower()
    score = fuzz.token_set_ratio(content, keyword)
    return score >= ratio


def match_in_file(filepath, keyword, ratio):
    """Search file at the given file path for the given keyword with the given
    ratio."""
    with open(filepath, encoding="utf-8") as file:
        content = file.read()
        return match(content, keyword, ratio)


def match_in_directory(dirpath, keyword, ratio):
    """Walk through the files in the directory path, check which files contain
    the given keyword and return the files in the directory that match the
    keyword for the given ratio."""
    matching_files = []
    for root, _, files in os.walk(dirpath):
        for name in files:
            filepath = os.path.join(root, name)
            if match_in_file(filepath, keyword, ratio):
                matching_files.append(filepath)

    matching_files.sort()
    return matching_files
